extends Node2D

onready var navigation = get_node_or_null("Navigation2D")
onready var tile_map = get_node_or_null("Navigation2D/TileMap")
onready var upper_bound = get_node_or_null("UB")
onready var lower_bound = get_node_or_null("LB")

export var id : String
var entrances : Array # this will change 
var exits : Array
var center : bool


func _ready():
	if not navigation:
		print('Could not get navigation node')
	if not tile_map:
		print('Could not get tile map node')


func get_id() -> String:
	return id


func get_bounds() -> Array:
	return [upper_bound.global_position, lower_bound.global_position]


func get_random_point(global:= true) -> Vector2:
	var cells = tile_map.get_used_cells_by_id(2)
	var cell = tile_map.map_to_world(cells[randi()%cells.size()])
	var cell_pos = cell
	if global:
		cell_pos = tile_map.to_global(cell)
	return cell_pos


func get_distant_point(pos, distance):
	var cells = tile_map.get_used_cells_by_id(2)
	var points = []
	for cell in cells:
		points.append(tile_map.to_global(tile_map.map_to_world(cell)))
	points.shuffle()
	for point in points:
		if pos.distance_squared_to(point) >= distance*distance:
			return point
	print('uau como que nao retornou no for wtf, get_distant_point deve estar quebrada')
	return points[0]


func array_to_global(array: Array) -> PoolVector2Array:
	var new_array = PoolVector2Array()
	for point in array:
		new_array.append(to_global(point))
	return new_array
