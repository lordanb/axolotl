extends Node

var rooms : Dictionary = {}
var current : String
var previous : String

signal room_load(id)
signal room_unload(id)
signal load_player(id)
signal refresh_camera()
signal spawn_object(id)


func _ready():
	var room_input = read_room_file()
	start_rooms(room_input)


func start_rooms(input : Array) -> void:
	for room_info in input:
		var id = room_info[0] as String
		var center = room_info[1] as bool
		var ngbs = room_info[2] as Array
		var convs = array_to_int(room_info[3])
		var sizegroup = room_info[4] as int
		add_room(id, center, ngbs, convs, sizegroup)


func add_room(id, center, neighbours, conveyors, size_group) -> void:
	var new_room = {'center': center,  'neighbours': neighbours, 
			'conveyors': conveyors, 'size_group': size_group, 'loaded': false}
	new_room.resource = get_room_resource(id) 
	rooms[id] = new_room


func read_room_file() -> Array:
	var file = File.new()
	file.open("res://Managers/RoomSource.json", File.READ)
	var parse = JSON.parse(file.get_as_text()).result
	file.close()
	return parse


func get_room_resource(id, sandbox = true) -> PackedScene:
	var room : PackedScene
	if sandbox:
		room = load("res://Levels/Sandbox/"+id+".tscn")
	else:
		room = load("res://Levels/"+id+".tscn")
	return room


func array_to_int(a : Array) -> Array:
	for i in range(a.size()):
		a[i] = a[i] as int
	return a


func is_loaded(id) -> bool:
	return rooms.get(id).loaded


func is_center(id) -> bool:
	return rooms.get(id).center


func get_neighbours(id) -> Array:
	return rooms.get(id).neighbours


func get_conveyors(id) -> Array:
	return rooms.get(id).conveyors


func get_conveyor_destination(origin, destination) -> int:
	var conv = 0
	var conv_from = RoomTable.get_conveyors(origin)
	var conv_to = RoomTable.get_conveyors(destination)
	for conv_id in conv_from:
		if conv_to.has(-conv_id):
			conv = -conv_id
	return conv


func get_room_destination(room, conv_to) -> String:
	var neighbours = get_neighbours(room)
	var destination : String
	for n in neighbours:
		var convs = get_conveyors(n)
		if convs.has(conv_to):
			destination = n
			break
	return destination


func get_size_group(id) -> int:
	return rooms.get(id).size_group


func get_room_instance(id) -> PackedScene:
	return rooms.get(id).resource


func toggle_room_aesthetics(id : String, value : bool) -> void:
	for creature in CreatureTable.get_room_creatures(id):
		var critter = CreatureTable.get_creature_instance(creature)
		critter.set_aesthetics(value)


func init_player(id) -> void:
	load_room(id)
	yield(get_tree(), "idle_frame")
	current = id
	toggle_room_aesthetics(id, true)
	var neighbours = get_neighbours(id)
	for neighbour in neighbours:
		load_room(neighbour)
	emit_signal("load_player", id)
	emit_signal("refresh_camera")
	emit_signal("spawn_object", id)


func move_player(id_to) -> void:
	previous = current
	toggle_room_aesthetics(previous, false)
	current = id_to
	toggle_room_aesthetics(current, true)
	print("MOVENDO DE ", previous, " PARA ", current)
	var prev_neighbours = get_neighbours(previous).duplicate(true)
	var cur_neighbours = get_neighbours(current)
	prev_neighbours.erase(current)
	print("OS VIZINHOS DO ROOM ANTERIOR SAO ", prev_neighbours)
	for room in prev_neighbours:
		unload_room(room)
	for room in cur_neighbours:
		load_room(room)
	emit_signal("refresh_camera")
	emit_signal("spawn_object", current)


func load_room(id) -> void:
	print('loading room ', id)
	if is_loaded(id):
		print('room already loaded, aborting...')
		return
	emit_signal("room_load", id)
	rooms.get(id).loaded = true


func unload_room(id) -> void:
	print('unloading room ', id)
	emit_signal("room_unload", id)
	rooms.get(id).loaded = false


func get_unloaded_rooms() -> Array:
	var unloaded_rooms = []
	for room in rooms:
		if not rooms.get(room).loaded:
			unloaded_rooms.append(room)
	return unloaded_rooms


func unload_all() -> void:
	for room in rooms.keys():
		if is_loaded(room):
			unload_room(room)


func pretty_print() -> void:
	print("-------------- TABELA DE QUARTOS ATUAL -----------------")
	for r in rooms.keys():
		print(r, "   ", rooms.get(r))
	print("--------------------------------------------------------")
