extends Node

var creatures : Dictionary  = {}
var creature_scenes  : Dictionary = {}

signal spawn_creature(id, room_from, room_to)
signal despawn_creature(id, room)
signal creature_moved(instance, room_from, room_to)

func _ready():
#	seed("gleba".hash())
	print("Creature table ready :)")


func start() -> void:
	if not creatures.empty():
		return
	load_creatures()
	var creature_input = read_creature_file()
	start_creatures(creature_input)
	pretty_print()


func reset() -> void:
	creatures = {}
	pretty_print()


func load_creatures() -> void:
	creature_scenes['BARRACUDA'] = load('res://Entities/Barracuda/Barracuda.tscn')
	creature_scenes['JELLY'] = load('res://Entities/Jelly/Jelly.tscn')


func read_creature_file() -> Array:
	var file = File.new()
	file.open("res://Managers/CreatureSource.json", File.READ)
	var parse = JSON.parse(file.get_as_text()).result
	file.close()
	return parse


func start_creatures(input):
	for creature_info in input:
		var id = creature_info[0] as int
		var room = creature_info[1]
		var creature = creature_scenes.get(creature_info[2])
		add_creature(id, room, creature.instance())


func add_creature(id, room, instance) -> void:
	# talvez seja mais inteligente gerar o id aqui?
	# isso seria um problema se a criature tiver que saber o id em geral?
	var new_creature = {'room': room, 'specimen': instance}
	creatures[id] = new_creature


func remove_creature(id) -> bool:
	# this shouldn't really happen but hey
	# maybe this happens when something lineages?
	return creatures.erase(id)


func get_creature_instance(id) -> Node2D:
	return creatures.get(id).specimen


func get_room_id(id) -> String:
	return creatures.get(id).room


func update_room(id, room) -> void:
	var creature = creatures.get(id)
	if not creature:
		print("Sem criatura de id ", id, " no update room, algo deu errado")
		return
	creature.room = room


func update_health(id, health) -> void:
	var creature = creatures.get(id).specimen
	if not creature:
		print("Sem criatura de id ", id, " no update_health, algo deu errado")
		return
	creature.set_health(health)
	return


func is_dead(id) -> bool:
	var creature = creatures.get(id).specimen
	if not creature:
		print("Sem criatura de id ", id, " no is_dead, deve estar morta msm")
	return creature.is_dead()


func get_room_creatures(room) -> Array:
	var room_creatures = []
	for id in creatures:
		if get_room_id(id) == room:
			room_creatures.append(id)
	return room_creatures


func get_type_creatures(type) -> Array:
	var type_creatures = []
	for id in creatures:
		var creature = creatures.get(id).specimen
		if creature.get_species() == type:
			type_creatures.append(id)
	return type_creatures


func get_id_from_instance(instance) -> int: 
	var instance_id : int
	for id in creatures.keys():
		if creatures.get(id).specimen == instance:
			instance_id = id
	return instance_id


func creature_instance_moved(instance, room_from, room_to) -> void:
	var id = get_id_from_instance(instance)
	update_room(id, room_to)
	emit_signal("creature_moved", instance, room_from, room_to)


func decide_action(id) -> String:
	if is_dead(id):
		#print('vc ta morta fica quieta')
		return ""
	var creature = creatures.get(id).specimen
	var actions = get_action_list(creature.get_species())
	var action_val = randf()
	var chosen_action = ""
	for a in actions:
		if action_val <= actions[a]:
			chosen_action = a
			break 
	return chosen_action


func get_action_list(creature_type) -> Dictionary:
	var action_list = {}
	match creature_type:
		'BARRACUDA':
			action_list = {'move': 0.3, 'attack': 0.55, 'idle': 1}
		'JELLY':
			action_list = {'move': 0.3,'idle': 1}
		_:
			action_list = {'idle': 1}
	return action_list


func run_creatures(id_list) -> void:
	#this is where targets are decided, since we already have a room list
	var action_list = []
	for creature in id_list:
		action_list.append(decide_action(creature))
	for i in range(id_list.size()):
		#not all actions have targets, but I think this is better than
		#putting an 'if' here
		var target_list = id_list.duplicate()
		target_list.remove(i)
		var target = "" if target_list.size() == 0 else target_list[randi()%target_list.size()]
		run_action(id_list[i], action_list[i], target)


func run_action(id, action, target_id) -> void:
	var creature = creatures.get(id)
	var target = creatures.get(target_id)
	match action:
		'move':
			var origin_room = creature.room
			var room_possibilities = RoomTable.get_neighbours(origin_room)
			#print("As possibilidades de movimentacao sao: ", room_possibilities)
			var destination = room_possibilities[randi()%room_possibilities.size()]
			update_room(id, destination)
			#print(id, " moveu para ", destination)
			if RoomTable.is_loaded(destination):
				emit_signal("spawn_creature", id, origin_room, destination)
		'attack':
			if target:
				if not target.specimen.is_dead():
					target.specimen.take_damage(1)
					#print(id, " atacou ", target_id)
		'idle':
			pass
			#print(id, " batatou")


func leave_room_to_table(instance, room, conv_to) -> void:
	var id = get_id_from_instance(instance)
	var room_to = RoomTable.get_room_destination(room, conv_to)
	print("hora de a b s t r a i r uma criatura de ", room, " para ", room_to)
	emit_signal("despawn_creature", instance, room)
	update_room(id, room_to)


func pretty_print() -> void:
	print("------------- TABELA DE CRIATURAS ATUAL ----------------")
	for id in creatures.keys():
		print(id, "  ", creatures.get(id))
	print("--------------------------------------------------------")
