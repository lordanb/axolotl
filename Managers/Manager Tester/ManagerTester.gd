extends Node

func _ready():
	#start_ct_tests()
	start_rm_tests()

func start_ct_tests():
	print("Testes da tabela de criaturas eee")
	CreatureTable.pretty_print()

	
func start_rm_tests():
	print("Testes da tabela de quartos eee")
	RoomTable.pretty_print()

func _on_Timer_timeout(test = false):
	if test:
		return
	var rooms = RoomTable.get_unloaded_rooms()
	for room in rooms:
		var room_creatures = CreatureTable.get_room_creatures(room)
		CreatureTable.run_creatures(room_creatures)
