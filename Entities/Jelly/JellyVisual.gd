extends Position2D

export (Array, NodePath) var T1 = []
export (Array, NodePath) var T2 = []
export (Array, NodePath) var T3 = []

const FABRIK = preload("res://Scripts/Fabrik.gd")

onready var tent1 = $Tentacle1
onready var tent2 = $Tentacle2
onready var tent3 = $Tentacle3
onready var endpoint1 = $T1J2
onready var endpoint2 = $T2J2
onready var endpoint3 = $T3J2
onready var target1 = $T1Target
onready var target2 = $T2Target
onready var target3 = $T3Target

onready var rflap = $RFlapJoint
onready var lflap = $LFlapJoint
onready var t_flap = $FlapTween

var tentacle1 = []
var tentacle2 = []
var tentacle3 = []
var t_segments := [45.0, 45.0, 45.0]
var ik
var rest := [Vector2(-45, 75), Vector2(5, 85), Vector2(25, 70)]


func _ready():
	ik = FABRIK.new()
	tentacle1 = get_node_array(T1)
	tentacle2 = get_node_array(T2)
	tentacle3 = get_node_array(T3)
	t_segments[0] += rand_range(-3.0, 3.0)
	t_segments[1] += rand_range(-4.0, 4.0)
	t_segments[2] += rand_range(-2.0, 2.0)
	change_rests()


func change_rests() -> void:
	for i in range(rest.size()):
		rest[i] += get_SRV(10.0) 


func update_targets(cur_dir: Vector2, idle : bool) -> void:
	if idle:
		set_targets(rest[0], rest[1], rest[2])
	else:
		var max_point_1 = cur_dir.normalized()*t_segments[0] + get_SRV(5.0)
		var max_point_2 = cur_dir.normalized()*t_segments[1] + get_SRV(5.0)
		var max_point_3 = cur_dir.normalized()*t_segments[2] + get_SRV(5.0)
		set_targets(max_point_1, max_point_2, max_point_3)


func update_tentacles() -> void:
	var pos_1 = PoolVector2Array()
	var pos_2 = PoolVector2Array()
	var pos_3 = PoolVector2Array()
	
	for i in range(tentacle1.size()):
		pos_1.append(tentacle1[i].global_position)
		pos_2.append(tentacle2[i].global_position)
		pos_3.append(tentacle3[i].global_position)
	
	var next_pos_1 = ik.fabrik(pos_1, [t_segments[0], t_segments[0]], endpoint1.global_position)
	var next_pos_2 = ik.fabrik(pos_2, [t_segments[1], t_segments[1]], endpoint2.global_position)
	var next_pos_3 = ik.fabrik(pos_3, [t_segments[2], t_segments[2]], endpoint3.global_position)
	
	for i in range(tentacle1.size()):
		tentacle1[i].global_position = next_pos_1[i]
		tentacle2[i].global_position = next_pos_2[i]
		tentacle3[i].global_position = next_pos_3[i]
	
	endpoint1.global_position = next_pos_1[2]
	endpoint2.global_position = next_pos_2[2]
	endpoint3.global_position = next_pos_3[2]
	update_tentacles_visual()


func update_tentacles_visual() -> void:
	#this is important because it gets the local position
	var pos_1 := PoolVector2Array()
	var pos_2 := PoolVector2Array()
	var pos_3 := PoolVector2Array()
	for i in range(tentacle1.size()):
		pos_1.append(tentacle1[i].position)
		pos_2.append(tentacle2[i].position)
		pos_3.append(tentacle3[i].position)
	tent1.points = pos_1
	tent2.points = pos_2
	tent3.points = pos_3


func move_towards_targets(delta) -> void:
	var to_target1 = target1.position - tentacle1[2].position
	var to_target2 = target2.position - tentacle2[2].position
	var to_target3 = target3.position - tentacle3[2].position
	endpoint1.position = endpoint1.position + to_target1*delta
	endpoint2.position = endpoint2.position + to_target2*delta
	endpoint3.position = endpoint3.position + to_target3*delta


func move_flaps(idle: bool) -> void:
	if idle:
		t_flap.interpolate_property(lflap, "position", lflap.position, 
				lflap.position + Vector2(0, 20), 0.9, Tween.TRANS_LINEAR, 
				Tween.EASE_OUT)
		t_flap.interpolate_property(rflap, "position", rflap.position, 
				rflap.position + Vector2(0, 20), 0.9, Tween.TRANS_LINEAR, 
				Tween.EASE_OUT)
		t_flap.start()
	else:
		t_flap.interpolate_property(lflap, "position", lflap.position, 
				lflap.position + Vector2(0, -20), 0.9, Tween.TRANS_LINEAR, 
				Tween.EASE_OUT)
		t_flap.interpolate_property(rflap, "position", rflap.position, 
				rflap.position + Vector2(0, -20), 0.9, Tween.TRANS_LINEAR, 
				Tween.EASE_OUT)
		t_flap.start()


func reset_flaps() -> void:
	t_flap.stop_all()
	t_flap.reset_all()
	lflap.position = Vector2(-50, 25)
	rflap.position = Vector2(50, 25)


func set_targets(t1, t2, t3) -> void:
	if target1.position != t1:
		target1.position = t1
	if target2.position != t2:
		target2.position = t2
	if target3.position != t3:
		target3.position = t3


func get_SRV(bound: float) -> Vector2: #Small Random Vector
	var x = rand_range(-bound, bound)
	var y = rand_range(-bound, bound)
	return Vector2(x, y)


func get_node_array(paths : Array) -> Array:
	# Receives a NodePath array and returns the correspondent nodes
	var a := []
	for i in paths:
		a.append(get_node(i))
	return a
