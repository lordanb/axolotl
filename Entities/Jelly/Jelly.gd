extends Creature

onready var sprite = $Sprite

var wobblyness : float
var speed : float = 3
var gravity : float = 0.95
var velocity : Vector2 = Vector2.ZERO
var idle : bool = true setget set_idle

onready var wait_timer = $WaitingTimer
onready var move_timer = $MovingTimer


func _ready():
	wobblyness = rand_range(0.1, 3.7)
	wait_timer.wait_time = rand_range(2.7, 3.4)
	move_timer.wait_time = rand_range(1.0, 1.24)
	wait_timer.start()


func set_toggled(t: bool) -> void:
	toggled = t
	sprite.reset_flaps()


func set_idle(a: bool) -> void:
	idle = a
	if idle:
		if toggled: sprite.change_rests()
	if toggled:
		sprite.move_flaps(idle)
		sprite.update_targets(-velocity, idle)


func _process(delta):
	if alive:
		if wait_timer.is_stopped() and idle:
			self.idle = false
			move_timer.start()
		if move_timer.is_stopped() and not idle:
			self.idle = true
			wait_timer.start()
		
		if idle:
			speed = 0 #wobblyness code goes here later I guess
		else:
			speed = 3
		var collision = move_and_collide(speed*velocity)
		if collision:
			velocity = velocity.bounce(collision.normal)
			sprite.update_targets(velocity, idle)
		
		if toggled:
			sprite.move_towards_targets(delta)
			sprite.update_tentacles()


func bitten(bite_damage):
	take_damage(bite_damage)
	if not is_dead():
		die()


func attacked():
	if not is_dead():
		die()

func on_Creature_Entered_Conv() -> void:
	var warped = ask_for_warp()
	if warped: print('sou uma jelly e fui pra outra sala')


func _on_WaitingTimer_timeout():
	velocity = Vector2(rand_range(-1, 1), rand_range(-1, 1))


func _on_MovingTimer_timeout():
	velocity = Vector2(rand_range(-1, 1), rand_range(-1, 1))
