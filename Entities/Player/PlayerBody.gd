extends Entity

onready var coyote_timer = $CoyoteTimer
onready var wall_coyote_timer = $WallCoyoteTimer
onready var fallthrough_area = $FallThroughArea
onready var ledgeholding_area = $LedgeHoldingArea
onready var pickup_area = $PickupArea
onready var aesthetics = $PlayerSprite

enum {IDLE, WALK, JUMP, FALL, SLIDE, HANG}

# For comparisons
const EPSILON := .2

# Jump constants
const JUMP_HEIGHT := 180.0
const JUMP_DURATION := 0.7

# Speed constants
const SPEED := 500.0
const MAX_FALL_SPEED := 1000.0
const SLIDE_SPEED := 150.0
const WALL_JUMP_SPEED := 800.0

# Acceleration constants
const ACCEL := 18.0
const AIR_ACCEL := 12.0
const FRICTION := 24.0
const WALL_JUMP_FRICTION := 2.4
const AIR_FRICTION := 6.0
const WJMS := 6.0 # Wall Jump Momentum Smoothing

var gravity : float
var jump_speed : float
var velocity := Vector2.ZERO
var state : int
var wall_jump_active := false
var last_wall_normal : Vector2
var air_acceleration := AIR_ACCEL
var facing_direction := 1.0
var current_ledge : Ledge
var current_object : PickableObject

var health : int = 1 setget set_health


func _ready():
	gravity = 2 * JUMP_HEIGHT / pow(JUMP_DURATION / 2, 2)
	jump_speed = - 2 * JUMP_HEIGHT / (JUMP_DURATION / 2)
	change_state(IDLE)
	self.health = 5


func set_health(h: int) -> void:
	if h > 5:
		health = 5
	elif h < 0:
		health = 0
		$DeathTimer.start()
	else:
		health = h
	aesthetics.update_sustenance_tank(health)


func _physics_process(delta):
	process_velocity(delta)
	process_actions()
	velocity = move_and_slide(velocity, Vector2.UP)
	check_states()
	
	if current_object:
		current_object.global_position = global_position
	
	if is_on_wall():
		for i in get_slide_count():
			var collision = get_slide_collision(i)
			if collision.collider is TileMap:
				if collision.normal.y == 0:
					last_wall_normal = collision.normal
					break


func process_velocity(delta):
	# Horizontal velocity
	var direction = Input.get_action_strength("right") -\
			Input.get_action_strength("left")
	if direction == 0:
		if is_on_floor():
			velocity.x = lerp(velocity.x, 0, FRICTION * delta)
		else:
			var f = WALL_JUMP_FRICTION if wall_jump_active else AIR_FRICTION
			air_acceleration = lerp(air_acceleration, f, WJMS * delta)
			velocity.x = lerp(velocity.x, 0, air_acceleration * delta)
	else:
		if is_on_floor():
			velocity.x = lerp(velocity.x, SPEED * direction, ACCEL * delta)
		elif state == HANG:
			if sign(last_wall_normal.x) == sign(direction):
				change_state(FALL)
		else:
			var f = WALL_JUMP_FRICTION if wall_jump_active else AIR_ACCEL
			air_acceleration = lerp(air_acceleration, f, WJMS * delta)
			velocity.x = lerp(velocity.x, SPEED * direction, air_acceleration * delta)
	
	# Vertical velocity
	if state != HANG:
		velocity.y = min(velocity.y + gravity * delta, MAX_FALL_SPEED)
		if state == SLIDE and velocity.y > SLIDE_SPEED:
			velocity.y = SLIDE_SPEED
	else:
		velocity.y = 0
		position.y = lerp(position.y, current_ledge.global_position.y -\
				ledgeholding_area.position.y, ACCEL * delta)


func process_actions():
	if Input.is_action_just_pressed("up"):
		if is_on_floor() or state == HANG or not coyote_timer.is_stopped():
			jump()
		elif is_on_wall() or not wall_coyote_timer.is_stopped():
			wall_jump()
	elif Input.is_action_just_pressed("down"):
		if state == HANG:
			change_state(FALL)
		elif is_on_platform():
			position.y += 1
	
	if Input.is_action_just_pressed("pickup") and not current_object:
		for body in pickup_area.get_overlapping_bodies():
			if body is PickableObject:
				current_object = body
				body.on_pickup()
				break
	elif Input.is_action_just_pressed("throw") and current_object:
		current_object.on_throw(facing_direction)
		current_object = null
	elif Input.is_action_just_pressed("letgo") and current_object:
		current_object.on_let_go(velocity)
		current_object = null
	elif Input.is_action_just_pressed("conveyor_call"):
		var warped = ask_for_warp()
		if warped:
			print('skidoodle')
		else:
			print("agora não bro")


func check_states():
	if velocity.x:
		facing_direction = sign(velocity.x)
	
	if velocity.y > 0:
		if is_on_wall():
			if current_ledge and not current_ledge.occupied:
				change_state(HANG)
			else:
				change_state(SLIDE)
		else:
			change_state(FALL)
	elif is_on_floor():
		if abs(velocity.x) > EPSILON:
			change_state(WALK)
		else:
			change_state(IDLE)


func change_state(new_state: int):
	if state == new_state:
		return
	
	var prev_state = state
	state = new_state
	
	match prev_state:
		JUMP:
			wall_jump_active = false
		HANG:
			if current_ledge:
				current_ledge.occupied = false
	
	match state:
		IDLE:
			$Sprite/Label.text = "IDLE"
		WALK:
			$Sprite/Label.text = "WALK"
		JUMP:
			$Sprite/Label.text = "JUMP"
		FALL:
			$Sprite/Label.text = "FALL"
			if prev_state == WALK:
				coyote_timer.start()
			elif prev_state == SLIDE or prev_state == HANG:
				wall_coyote_timer.start()
		SLIDE:
			$Sprite/Label.text = "SLIDE"
			aesthetics.set_facing(facing_direction)
		HANG:
			$Sprite/Label.text = "HANG"
			if current_ledge:
				current_ledge.occupied = true
				aesthetics.set_facing_ledge(current_ledge.facing)
	aesthetics.transition_arms(state)


func is_on_platform():
	if not is_on_floor():
		return false
	
	for body in fallthrough_area.get_overlapping_bodies():
		if body is Platform:
			return true
	return false


func jump():
	velocity.y = jump_speed
	coyote_timer.stop()
	change_state(JUMP)


func wall_jump():
	velocity.x = last_wall_normal.x * WALL_JUMP_SPEED
	jump()
	wall_jump_active = true
	air_acceleration = WALL_JUMP_FRICTION
	wall_coyote_timer.stop()


func bitten(dmg: int) -> void:
	self.health = health-dmg


func _on_LedgeHoldingArea_area_entered(area):
	if area is Ledge and not current_ledge:
		current_ledge = area


func _on_LedgeHoldingArea_area_exited(area):
	if area == current_ledge:
		current_ledge.occupied = false
		current_ledge = null
		if state == HANG:
			change_state(FALL)


func _on_DeathTimer_timeout():
	get_parent().quit_game()
