extends Node2D

enum {IDLE, WALK, JUMP, FALL, SLIDE, HANG}

onready var player_body = get_parent()
onready var head = $Head
onready var la_target = $LArmTarget
onready var ra_target = $RArmTarget
onready var left_arm  = $LeftArm
onready var right_arm = $RightArm

onready var l_sus = $HeadJoint/TorsoJoint/LSus
onready var r_sus = $HeadJoint/TorsoJoint/RSus
onready var lb_sus = $HeadJoint/TorsoJoint/LBSus
onready var rb_sus = $HeadJoint/TorsoJoint/RBSus

onready var trans_tween = $Tweens/TranstionTween
onready var sus_tween = $Tweens/SustenanceTween
var tween_start : Dictionary

export (Array, NodePath) var body_nodes = []
export (Array, NodePath) var rarm_nodes = []
export (Array, NodePath) var larm_nodes = []

const IK = preload("res://Scripts/Fabrik.gd")
const ARM_LENGTH := [25.0, 25.0]
const SUS_TANK:= {'l': 3.6, 'r': 7.2}

var body = []
var r_arm = []
var l_arm = []
var ik
var facing_right_ledge := true # this disgusts me but I have no time
var facing_right := true

func _ready():
	body = get_node_array(body_nodes)
	r_arm = get_node_array(rarm_nodes)
	l_arm = get_node_array(larm_nodes)
	setup_tweens()
	update_arms_visual()
	ik = IK.new()


func setup_tweens() -> void:
	tween_start[IDLE] = {'l': Vector2(-35, 64), 'r': Vector2(35, 64)}
	tween_start[WALK] = {'l': Vector2(-20, 60), 'r': Vector2(20, 60)}
	tween_start[JUMP] = {'l': Vector2(-40, -27), 'r': Vector2(40, -27)}
	tween_start[FALL] = {'l': Vector2(-41, -27), 'r': Vector2(41, -27)}
	tween_start[SLIDE] = {'l': Vector2(-35, 64), 'r': Vector2(35, 64)}
	tween_start[HANG] = {'l': Vector2(-35, 64), 'r': Vector2(35, 64)}


func adjust_tween(state: int) -> void:
	match state:
		HANG:
			if facing_right_ledge:
				tween_start[HANG] = {'l': Vector2(30, 5), 'r': Vector2(30, 8)}
			else:
				tween_start[HANG] = {'l': Vector2(-30, 5), 'r': Vector2(-30, 8)}


func update_arms_positions() -> void:
	var r_pos = PoolVector2Array()
	var l_pos = PoolVector2Array()
	for i in range(r_arm.size()):
		r_pos.append(r_arm[i].global_position)
		l_pos.append(l_arm[i].global_position)
	
	var next_r_pos = ik.fabrik(r_pos, ARM_LENGTH, get_ra_target_pos())
	var next_l_pos = ik.fabrik(l_pos, ARM_LENGTH, get_la_target_pos())
	
	for i in range(0, r_arm.size()):
		r_arm[i].global_position = next_r_pos[i]
		l_arm[i].global_position = next_l_pos[i]


func update_arms_visual() -> void:
	var r_pos = PoolVector2Array()
	var l_pos = PoolVector2Array()
	for i in range(r_arm.size()):
		r_pos.append(r_arm[i].position)
		l_pos.append(l_arm[i].position)
	right_arm.points = r_pos
	left_arm.points  = l_pos


func set_arms_position(l: Vector2, r: Vector2) -> void:
	la_target.position = l
	ra_target.position = r


func transition_arms(state: int) -> void:
	trans_tween.interpolate_property(la_target, "position", 
			la_target.position, tween_start[state].l, 
			0.1337, Tween.TRANS_SINE, Tween.EASE_IN)
	trans_tween.interpolate_property(ra_target, "position", 
			ra_target.position, tween_start[state].r, 
			0.1337, Tween.TRANS_SINE, Tween.EASE_IN)
	trans_tween.start()


func set_facing_ledge(facing: int) -> void:
	if facing == 0: # facing right
		facing_right_ledge = true
	else: # facing left
		facing_right_ledge = false


func set_facing(facing: int) -> void:
	if facing == 1:
		facing_right = true
	else:
		facing_right = false


func get_la_target_pos() -> Vector2:
	return la_target.global_position


func get_ra_target_pos() -> Vector2:
	return ra_target.global_position


func _process(_delta):
	var state = player_body.state
	match state:
		IDLE:
			body[-1].rotation = 0
			if not trans_tween.is_active():
				set_arms_position(Vector2(-35, 64), Vector2(35, 64))
		WALK:
			body[-1].rotation = 0
			if not trans_tween.is_active():
				set_arms_position(Vector2(-20, 60), Vector2(20, 60))
		JUMP:
			body[-1].rotation = 0
			if not trans_tween.is_active():
				set_arms_position(Vector2(-42, -45), Vector2(42, -45))
		FALL:
			body[-1].rotation = 0
			if not trans_tween.is_active():
				set_arms_position(Vector2(-30, -28), Vector2(30, -28))
		SLIDE:
			if facing_right:
				body[-1].rotation = deg2rad(-45)
			else:
				body[-1].rotation = deg2rad(45)
			if not trans_tween.is_active():
				if facing_right:
					set_arms_position(Vector2(-26, 70), Vector2(50, 20))
				else:
					set_arms_position(Vector2(-50, 20), Vector2(26, 70))
		HANG:
			if facing_right:
				body[-1].rotation = deg2rad(-45)
			else:
				body[-1].rotation = deg2rad(45)
			if not trans_tween.is_active():
				if facing_right_ledge:
					set_arms_position(Vector2(-50, 50), Vector2(45, -1))
				else:
					set_arms_position(Vector2(-45, -1), Vector2(50, 50))
	update_arms_positions()
	update_arms_visual()


func update_sustenance_tank(amount: int) -> void:
	var l_amt : Vector2 = lb_sus.position + Vector2(0,-amount*SUS_TANK.l)
	var r_amt : Vector2 = rb_sus.position + Vector2(0,-amount*SUS_TANK.r)
	var l_time := rand_range(0.7,1.5)
	var r_time := rand_range(0.7,1.5)
	sus_tween.interpolate_property(l_sus, 'position', l_sus.position,
			l_amt, l_time, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	sus_tween.interpolate_property(r_sus, 'position', r_sus.position,
			r_amt, r_time, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	sus_tween.start()


func get_node_array(paths : Array) -> Array:
	# Receives a NodePath array and returns the correspondent nodes
	var a := []
	for i in paths:
		a.append(get_node(i))
	return a
