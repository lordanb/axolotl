extends KinematicBody2D
class_name Entity

var conveyor : Node2D = null setget set_conveyor
signal entered_conveyor


func set_conveyor(c: Node2D) -> void:
	conveyor = c
	if at_conveyor():
		emit_signal("entered_conveyor")


func ask_for_warp() -> bool:
	if not at_conveyor():
		return false
	var warped = conveyor.warp(self)
	if warped: # I can just return warped but idk
		return true
	return false

func at_conveyor() -> bool:
	if not conveyor:
		return false
	return true
