extends Position2D

enum states {IDLE, WANDER, ANGRY, AFRAID}

const THETA : = PI/3
const EPSILON : = deg2rad(2.0)

onready var barracuda = get_parent() # Barracuda reference
export (Array, NodePath) var joint_nodes = []
export (Array, NodePath) var tail_nodes = []

onready var upper_tail = $TorsoJoint0/TorsoJoint1/TorsoJoint2/TailJoint0/TailJoint1/Tail1/TE0_2
onready var lower_tail = $TorsoJoint0/TorsoJoint1/TorsoJoint2/TailJoint0/TailJoint1/Tail1/TE1_2

var gravity : = 0.0
var wiggle_up : = true
var p : = 0.0 # como eu chamo isso

var joints = []
var tails = []


func _ready() -> void:
	for i in joint_nodes:
		joints.append(get_node(i))
	for i in tail_nodes:
		tails.append(get_node(i))


func _process(delta) -> void:
	increment_pendulum()
	var speed = barracuda.speed
	var health = float(barracuda.health)/float(barracuda.max_health)
	if barracuda.state != states.IDLE:
		for i in range(joints.size()):
			var joint = joints[i]
			var r = delta*THETA*speed*wiggle_direction(joint.rotation)*health
			if i == 0:
				# first one gets info from parent
				joint.rotation += rotation + r
			else:
				# any other one gets info from the one before
				var prev_joint = joints[i-1]
				joint.rotation = prev_joint.rotation + r
	else:
		for i in range(joints.size()):
			var joint = joints[i]
			var j_sign : = 1.0 if i%2 == 0 else -1.0
			if i == 0:
				joint.position.y += 2*delta*cos(p)*j_sign * (1.3-health)
			else:
				joint.position.y += 2*delta*cos(p)*j_sign * (1.3-health)
			if joint.rotation > EPSILON:
				joint.rotation -= THETA/2*delta
			elif joint.rotation < -EPSILON:
				joint.rotation += THETA/2*delta


func increment_pendulum() -> void:
	if p > 2*PI:
		p = 0
	else:
		p += PI/83


func wiggle_direction(cur_rotation : float) -> float:
	var bound = 21.0
	
	if cur_rotation >= deg2rad(bound):
		wiggle_up = false
	elif cur_rotation <= deg2rad(-bound):
		wiggle_up = true
	
	if wiggle_up:
		return 1.0
	return -1.0


func _on_Barracuda_personality_defined():
	for t in [upper_tail, lower_tail]:
		t.position.x -= 54/barracuda.restlessness
