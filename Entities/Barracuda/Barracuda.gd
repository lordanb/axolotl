extends Creature

enum states {IDLE, WANDER, ANGRY, AFRAID}

signal personality_defined()

onready var vision = $Head/Vision
onready var head = $Head
onready var follow_range = $Head/FollowRange
onready var t_inspect = $InspectionTimer
onready var t_head = $HeadMovingTimer
onready var t_bite = $BiteCooldownTimer
onready var t_flee = $RunAwayTimer
onready var t_wander = $WanderTimer
onready var label = $Label

var navigation : Node2D

var speed : float = 1
var gravity : int = 0
var velocity : = Vector2.ZERO
var acceleration : = Vector2.ZERO
var facing : float
var orientation : = Vector2.RIGHT
var reach : float = 50.0

var goal : Vector2 setget set_goal
var target = null
var path : PoolVector2Array setget set_path
var steer_force : = 5.0
const SAFE_RANGE : = 500 

const max_health : int = 5
var bite_dmg : int = 3 
var state : int setget change_state
var restlessness : float
var curiosity : float
var snappy : bool = true


func _ready():
	if not navigation:
		print('Eu nao sei me localizar e serei selecionada naturalmente')
	decide_personality()
	self.health = 5
	state = states.IDLE
	label.text = id as String
	t_inspect.start()
	vision.add_exception(self) #I don't think this works but hey, it's here


func update_room() -> void:
	#parent is and always will be Factory
	.update_room()
	navigation = cur_room.get_node_or_null("Navigation2D")

func decide_personality():
	restlessness = randi()%6+2
	curiosity = randi()%10+1
	print("Oi eu sou uma barracuda com restlessness ", restlessness,
	" e curiosity ", curiosity)
	t_flee.wait_time = 5 + restlessness/2 + rand_range(-0.5, 0.5)
	var t = rand_range(0.5, 0.9)
	t_inspect.wait_time = 12-curiosity
	t_wander.wait_time = t*(13-curiosity) + (1-t)*(9-restlessness)+5
	emit_signal("personality_defined")


func set_toggled(t: bool) -> void:
	toggled = t
	$Sprite.set_process(t)


func set_path(p: PoolVector2Array) -> void:
	path = p
#	var line = Line2D.new()
#	get_parent().add_child(line)
#	line.default_color = Color.red
#	line.points = path


func _process(delta):
	if alive:
		if vision.is_colliding():
			check_vision()
		match state:
			states.IDLE:
				if t_wander.is_stopped():
					t_wander.start()
				if not t_head.is_stopped():
					var head_r = head.rotation
					head.rotation = head_r + head_r*rotate_head()*delta
			states.WANDER:
				if path.size() == 0:
					change_state(states.IDLE)
				else:
					var cur_goal = path[0]
					if head.global_position.distance_to(cur_goal) <= reach:
						path.remove(0)
					else:
						adjust_velocity(cur_goal, delta)
						position += move_and_slide(velocity)
			states.ANGRY:
				if path.size() == 0:
					change_state(states.IDLE)
					look_for_prey()
				else:
					var cur_goal = path[0]
					if head.global_position.distance_to(cur_goal) <= reach:
						path.remove(0)
					else:
						adjust_velocity(cur_goal, delta)
						position += move_and_slide(velocity)
			states.AFRAID:
				if path.size() == 0:
					#lookaround goes here
					change_state(states.IDLE)
				else:
					var cur_goal = path[0]
					if head.global_position.distance_to(cur_goal) <= reach:
						path.remove(0)
					else:
						adjust_velocity(cur_goal, delta)
						position += move_and_slide(velocity)


func change_state(toState):
	if state == toState:
		return
	#leaving consequences
	match state:
		states.IDLE:
			var idle_timers = [t_inspect, t_wander]
			for timer in idle_timers:
				timer.stop()
			gravity = 0
		states.WANDER:
			t_wander.stop()
			path = PoolVector2Array([])
		states.ANGRY:
			target = null
			path = PoolVector2Array([])
		states.AFRAID:
			path = PoolVector2Array([])
	#entering consequences
	match toState:
		states.IDLE:
#			label.text = 'IDLE'
			var idle_timers = [t_inspect]
			for timer in idle_timers:
				timer.start()
			if orientation == Vector2.RIGHT:
				rotation = 0
			else:
				rotation = PI
			speed = 1
			state = toState
		states.WANDER:
#			label.text = "STROLLIN"
			self.goal = cur_room.get_random_point(false)
			self.path = get_path_to(goal)
			speed = 1
			state = toState
		states.ANGRY:
#			label.text = 'ANGY'
			speed = 3
			head.rotation = 0
			state = toState
		states.AFRAID:
#			label.text = 'AFRAID'
			speed = 4
			self.goal = cur_room.get_distant_point(cur_room.to_local(global_position), SAFE_RANGE)
			self.path = get_path_to(goal)
			t_flee.start()
			state = toState


func set_goal(point : Vector2):
	goal = point
	adjust_orientation()


func check_vision():
	var sighted = vision.get_collider()
	if sighted == null:
		return
	if sighted.is_in_group("barracudaPrey"):
		if state != states.AFRAID:
			focus(sighted)
	elif sighted.is_in_group("wall"):
		if sqrt(head.global_position.distance_squared_to(
				vision.get_collision_point())) < 115:
			if state == states.IDLE:
				flip()


func rotate_head() -> float:
	var theta = orientation.x*PI/9
	if t_head.time_left > 3:
		return -theta
	elif t_head.time_left > 1:
		return theta
	else:
		 return -theta


func adjust_velocity(cur_goal, delta):
	acceleration += seek(cur_goal)
	velocity += acceleration*delta
	velocity = velocity.clamped(speed)
	if abs(rotation-velocity.angle()) > 0.35:
		rotation = velocity.angle()


func adjust_orientation():
	var dir = goal-global_position
	if sign(dir.x) != sign(orientation.x):
		flip()


func flip():
	self.scale.x = -scale.x
	if orientation == Vector2.LEFT:
		orientation = Vector2.RIGHT
	else:
		orientation = Vector2.LEFT


func focus(new_target):
	if new_target == target: #look at this later and think about it
		return
	target = new_target
	self.goal = cur_room.to_local(target.global_position)
	change_state(states.ANGRY)
	var new_path = get_path_to(goal)
	if path.size() == 0:
		self.path = new_path
	elif path.size() >= new_path.size():
		self.path = new_path
		print('opa tem outra coisa mais perto bora') #still haven't seen this print once I am scared


func get_path_to(point):
	var path_to = navigation.get_simple_path(cur_room.to_local(global_position), point)
	return cur_room.array_to_global(path_to)


func seek(point):
	var acc = Vector2.ZERO
	var desired = (point - head.global_position).normalized() * speed
	acc = (desired - velocity).normalized() * steer_force
	return acc


func look_for_prey():
	var possible_prey = follow_range.get_overlapping_bodies()
	if possible_prey.size() > 0:
		possible_prey.shuffle()
		var prey
		for creature in possible_prey:
			if creature.is_in_group("barracudaPrey") and creature != self:
				prey = creature
				break
		if prey:
			focus(prey)


func bitten(dmg):
	if alive:
		damaged(dmg)


func attacked(dmg):
	if alive:
		damaged(dmg)


func damaged(dmg):
	if dmg > 0:
		health -= dmg
		var bias := float(health)/float(max_health)
		var destiny = 2*bias + 3*bias + randi()%6
		print("meu destino é ", destiny)
		if destiny < 3:
			die()
		elif destiny <= 7:
			change_state(states.AFRAID)
		else:
			#fazer ela chacoalhar idk
			print('ai')
	else:
		die()


func _on_BiteRange_body_entered(body):
	if body == self:
		return
	if body.is_in_group('barracudaPrey'):
		if snappy:
			snappy = false
			body.bitten(bite_dmg)
			t_bite.start()


func _on_InspectionTimer_timeout():
	t_head.start()


func _on_HeadMovingTimer_timeout():
	head.rotation = 0
	t_inspect.start()


func _on_RunAwayTimer_timeout():
	change_state(states.IDLE)


func _on_BiteCooldownTimer_timeout():
	snappy = true


func _on_WanderTimer_timeout():
	var t = rand_range(0.5, 0.9)
	t_wander.wait_time = t*(13-curiosity) + (1-t)*(9-restlessness)
	change_state(states.WANDER)


func _on_Barracuda_tree_entered():
	update_room()


func on_Creature_Entered_Conv() -> void:
	var warped = ask_for_warp()
	if warped:
		print("barracuda", id," mudou pra outro room tralala")
		change_state(states.IDLE)
