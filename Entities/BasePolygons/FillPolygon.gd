extends Polygon2D
class_name FillPolygon2D

export (Array, NodePath) var neighbor_vertexes
var vertexes := []

func _ready():
	for i in neighbor_vertexes.size():
		vertexes.append(get_node(neighbor_vertexes[i]))


func _process(_delta):
	var updated_vertexes = PoolVector2Array() 
	for i in vertexes.size():
		var vertex = to_local(vertexes[i].global_position)
		updated_vertexes.append(vertex)
	self.polygon = updated_vertexes
