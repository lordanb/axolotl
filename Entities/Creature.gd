extends Entity
class_name Creature

export(int) var health : int setget set_health, get_health
export(int) var threat : int setget set_threat, get_threat
export(String, "JELLY", "BARRACUDA") var species : String setget set_species, get_species

var id = null
var alive := true
var cur_room_id : String
var cur_room : Node2D #please don't break
var stasis_timer : Timer
var toggled := false setget set_toggled


func _ready():
	if not is_connected("entered_conveyor", self, "on_Creature_Entered_Conv"):
# warning-ignore:return_value_discarded
		connect("entered_conveyor", self, "on_Creature_Entered_Conv")


func set_health(h : int) -> void:
	health = h


func get_health() -> int:
	return health


func set_threat(t : int) -> void:
	threat = t


func get_threat() -> int:
	return threat


func set_species(s: String) -> void:
	species = s


func get_species() -> String:
	return species


func set_toggled(t: bool) -> void:
	toggled = t


func die() -> void:
	if not alive:
		return
	
	alive = false
	disable_physics()
	set_deferred("visible", false)


func is_dead() -> bool:
	return not alive


func take_damage(dmg : int) -> void:
	self.health = self.get_health() - dmg
	if health <= 0:
		die()


func get_global_point(point : Vector2) -> Vector2:
	var parent_pos = get_parent().global_position
	var global_pos = point + parent_pos
	return global_pos


func to_global_Vector2_array(array : PoolVector2Array) -> PoolVector2Array:
	var new_array = []
	for point in array:
		new_array.append(get_global_point(point))
	return PoolVector2Array(new_array)


func update_room() -> void:
	if id == null:
		id = CreatureTable.get_id_from_instance(self)
	cur_room_id = CreatureTable.get_room_id(id)
	cur_room = get_parent().get_slot_from_id(cur_room_id).get_child(0)


func set_aesthetics(value : bool) -> void:
	self.toggled = value


func disable_physics() -> void: #this means the creature died, I think
	var children = get_children()
	for child in children:
		if child is CollisionShape2D:
			child.set_deferred("disabled", true)


func on_Creature_Entered_Conv() -> void:
	print("Creature Entered Conv called, pwease ovewwide me uwu")
