tool
extends StaticBody2D
class_name Platform

export(Vector2) var size = Vector2(200, 20) setget set_size


func _ready():
	pass

func set_size(new_size: Vector2):
	size = new_size
	$Sprite.region_rect = Rect2(Vector2.ZERO, size)
	$CollisionShape2D.shape = $CollisionShape2D.shape.duplicate()
	$CollisionShape2D.shape.extents = size / 2
