extends RigidBody2D
class_name PickableObject

export var throw_impulse := Vector2(600, -400)
export var throw_damage := 1

const LET_GO_DAMPING = .5
const CREATURE_BIT = 3

var shapes : Array


func _ready():
	for child in get_children():
		if child is CollisionShape2D:
			shapes.append(child)


func set_shapes_disabled(var disabled: bool):
	for shape in shapes:
		(shape as CollisionShape2D).disabled = disabled


func on_pickup():
	set_shapes_disabled(true)
	set_mode(MODE_KINEMATIC)
	set_collision_mask_bit(CREATURE_BIT, false)


func on_throw(direction: float):
	var pos := global_position
	global_position = pos
	linear_velocity = Vector2.ZERO
	set_mode(MODE_RIGID)
	set_shapes_disabled(false)
	apply_central_impulse(Vector2(throw_impulse.x * direction, throw_impulse.y))
	set_collision_mask_bit(CREATURE_BIT, true)


func on_let_go(velocity: Vector2):
	var pos := global_position
	global_position = pos
	linear_velocity = velocity * LET_GO_DAMPING
	set_mode(MODE_RIGID)
	set_shapes_disabled(false)


func _on_PickableObject_body_entered(body):
	if body is Creature:
		(body as Creature).take_damage(throw_damage)
	set_collision_mask_bit(CREATURE_BIT, false)
