extends Node2D
class_name Conveyor

enum {LEFT, RIGHT}

onready var trigger_area = $TriggerArea
onready var arrow = $BG/ArrowIndicator
onready var timer = $Timer

export var id : int setget set_id
var sister_id : int
export var room_id : String
var warp_position : Vector2 

var available := true setget set_available

var sister : Node2D
var current_body = null


func _ready():
	if id == 0:
		print("TEM UMA CONVEYOR COM ID ZERO VAI DAR RUIM APERTA F8")
	warp_position = trigger_area.global_position
	update_sister() # this is not ideal but idk
	if sister:
		print("sou a conveyor ", id, " e mando pra conveyor ", sister.id)
	else:
		print("sou a conveyor ", id, " e mando pra tabela")


func update_sister() -> void:
	sister = get_sister()


func has_sister() -> bool:
	return (sister != null and is_instance_valid(sister))


func get_sister() -> Node2D:
	var convs = get_tree().get_nodes_in_group("conveyors") #bad
	var sis = null
	for conv in convs:
		if conv.id == sister_id:
			sis = conv
			break
	return sis


func set_id(i : int) -> void:
	id = i
	sister_id = -i


func set_available(value: bool) -> void:
	available = value
	yield(get_tree(), "idle_frame")
	if self.has_sister():
		sister.force_available(value)
		if sister.available:
			sister.arrow.color = Color.white
		else:
			sister.arrow.color = Color.brown
	if available:
		arrow.color = Color.white
	else:
		arrow.color = Color.brown


func force_available(value: bool) -> void:
	available = value


func reset_pair() -> void:
	var convs = [self]
	if self.has_sister():
		convs.append(sister)
	for conv in convs:
		conv.available = true
		current_body = null
	print('conveyors ', abs(id), "+- foram resetadas, obrigado e volte sempre")


func warp(entity) -> bool:
	if (not entity is Entity) or not available:
		print('oi perdao nao podemos atender agora tente de novo mais tarde')
		return false
	timer.start()
	self.available = false
	current_body = entity
	if self.has_sister():
		entity.conveyor = sister
	if entity is Creature:
		warp_creature(entity)
	else:
		# has to be the player! right...?
		entity.global_position = sister.warp_position
		RoomTable.move_player(sister.get_parent().get_id())
	return true


func warp_creature(creature) -> void:
	if self.has_sister():
		var room = get_parent().get_id()
		var neighbour = sister.get_parent().get_id()
		creature.global_position = sister.warp_position
		# change this to "update creature in table" or smth
		CreatureTable.creature_instance_moved(current_body, room, neighbour)
	else:
		CreatureTable.leave_room_to_table(current_body, room_id, sister_id)


func _on_Conveyor_tree_entered():
	update_sister()
	room_id = get_parent().get_id()


func _on_Timer_timeout():
	current_body = null
	self.available = true


func _on_TriggerArea_body_entered(body):
	if body is Entity:
		if not body is Creature:
			print("player in")
		body.conveyor = self


func _on_TriggerArea_body_exited(body):
	if body is Entity:
		if not body is Creature:
			print("player out")
		body.conveyor = null
