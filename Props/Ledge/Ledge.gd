tool
extends Area2D
class_name Ledge

onready var polygon = $Polygon2D

enum {LEFT, RIGHT}

export(int, "Left", "Right") var facing := 0 setget set_facing

var occupied := false

func _ready():
	polygon.scale.x = -1 if facing else 1

func set_facing(f: int):
	facing = f
	$Polygon2D.scale.x = -1 if facing else 1
