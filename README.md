# Axolotl

2D game based on exploration of an AI-controlled ecosystem.

Axolotl was made as part of MAC0499 - Capstone Project, our final computer science project at USP.

### Controls:

Move with the left and right arrow keys, jump with the up key and drop from ledges and platforms with the down key.

A - grab object
E - drop object
D - throw object
W - enter conveyor