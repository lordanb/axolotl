extends Node

# Sets of nodes that will be animated by this module should NOT follow the
# hierarchy of the tree. They should all be children of a same Node2D

const TOL := 1.0 


func fabrik(joints: Array, links: Array, target: Vector2) -> PoolVector2Array:
	# This algorithm is a GDScript recreation of Aristidou and Lasenby's
	# FABRIK algorithm, an iterative IK solver
	
	var n := joints.size()
	var dist_target = target.distance_to(joints[0])
	var total_link_length := float_array_sum(links)
	
	if dist_target > total_link_length:
		# oops can't reach that
		for i in range(0, n-1):
			var p = joints[i]
			var d = links[i]
			var r = target.distance_to(p)
			var lambda = d/r
			joints[i+1] = (1 - lambda) * p + lambda * target
	else:
		# yay I can reach this
		var b = joints[0]
		var dif = target.distance_to(joints[n-1])
		
		while dif > TOL:
			joints[n-1] = target
			
			for i in range(n-2, -1, -1): # forward reaching
				var pii = joints[i+1]
				var pi = joints[i]
				var d = links[i]
				var r = pii.distance_to(pi)
				var lambda = d/r
				joints[i] = (1 - lambda) * pii + lambda * pi
			
			joints[0] = b # assure the root stays the same
			
			for i in range(0, n-1): # backward reaching
				var pii = joints[i+1]
				var pi = joints[i]
				var d = links[i]
				var r = pii.distance_to(pi)
				var lambda = d/r
				joints[i+1] = (1 - lambda) * pi + lambda * pii
			
			dif = target.distance_to(joints[n-1])
	
	return PoolVector2Array(joints)


func float_array_sum(array: Array) -> float:
	var sum := 0.0
	for item in array:
		sum += item
	return sum
