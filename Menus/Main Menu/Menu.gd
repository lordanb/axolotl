extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Play_pressed():
	CreatureTable.start()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Factory/Factory.tscn")


func _on_Reset_Game_pressed():
	CreatureTable.reset()


func _on_Credits_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Menus/Credits Screen/Credits.tscn")
