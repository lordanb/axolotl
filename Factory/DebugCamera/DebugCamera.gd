extends Camera2D

func _ready():
	set_process_input(true)


func _input(event):
	if event is InputEventMouseButton:
		position = get_global_mouse_position()
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_O:
			self.zoom += Vector2(1, 1)
		if event.pressed and event.scancode == KEY_P and self.zoom > Vector2(1, 1):
			self.zoom -= Vector2(1, 1)
