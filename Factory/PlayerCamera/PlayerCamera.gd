extends Camera2D

var player : Node2D
var upper_bound : Vector2 setget _set_upper_bound
var lower_bound : Vector2 setget _set_lower_bound


func is_room_small() -> bool:
	var room_size := lower_bound-upper_bound
	if room_size.x < 1366 and room_size.y < 768:
		return true
	return false


func _set_upper_bound(bound: Vector2) -> void:
	upper_bound = bound
	limit_left = upper_bound.x as int
	limit_top = upper_bound.y as int


func _set_lower_bound(bound: Vector2) -> void:
	lower_bound = bound
	limit_right = lower_bound.x as int
	limit_bottom = lower_bound.y as int
