extends Node2D

onready var player_node = preload("res://Entities/Player/PlayerBody.tscn")

const BALL = preload("res://Props/PickableObject/Objects/Ball.tscn")
const STICK = preload("res://Props/PickableObject/Objects/Spear.tscn")

onready var slot0 = $Slot0
onready var slot1 = $Slot1
onready var slot2 = $Slot2
onready var slot3 = $Slot3
onready var slot4 = $Slot4
onready var creatures_timer = $OfflineCreaturesTimer
onready var camera = $Camera

var current_rooms : Array
export (bool) var test_mode
var test_count : int = 0
const MAX_ROOM_SIZE : int = 70
const TILE_SIZE : int = 64
var player : Node2D
var slot_available : Array

# this is temporary and will most likely be deleted in the future
var objects : Dictionary


func _ready() -> void:
	objects = {
		'S1': BALL,
		'S2': STICK,
		'S3': BALL,
		'S4': BALL, 
		'S5': BALL, 
		'S6': STICK,
		'S7': STICK,
	}
	# should we throw these connects in a func?
# warning-ignore:return_value_discarded
	RoomTable.connect("room_load", self, "room_load")
# warning-ignore:return_value_discarded
	RoomTable.connect("room_unload", self, "room_unload")
# warning-ignore:return_value_discarded
	RoomTable.connect("load_player", self, "spawn_player")
# warning-ignore:return_value_discarded
	RoomTable.connect("refresh_camera", self, "set_camera_bounds")
# warning-ignore:return_value_discarded
	RoomTable.connect("spawn_object", self, "spawn_object")
# warning-ignore:return_value_discarded
	CreatureTable.connect("spawn_creature", self, "spawn_creature")
# warning-ignore:return_value_discarded
	CreatureTable.connect("despawn_creature", self, "despawn_creature")
# warning-ignore:return_value_discarded
	CreatureTable.connect("creature_moved", self, "creature_moved")
	
	randomize()
	
	current_rooms = [slot0, slot1, slot2, slot3, slot4]
	slot_available = [true, true, true, true, true]
	set_slot_positions()
	
	if test_mode:
		camera.current = false
		$FactoryCamera.current = true
	else:
		$FactoryCamera.current = false
		camera.current = true

	RoomTable.init_player("S1")
	creatures_timer.start()


func spawn_player(id : String) -> void:
	var room = get_slot_from_id(id)
	var spawn_pos = room.get_child(0).get_node("PlayerSpawn").global_position
	player = player_node.instance()
	self.add_child(player)
	player.global_position = spawn_pos
	self.remove_child(camera)
	player.add_child(camera)


func set_camera_bounds() -> void:
	var room = get_slot_from_id(RoomTable.current)
	room = room.get_child(0)
	camera.upper_bound = room.get_node("UB").global_position
	camera.lower_bound = room.get_node("LB").global_position


func set_slot_positions() -> void:
	for i in range(current_rooms.size()):
		var pos = Vector2(i*TILE_SIZE*MAX_ROOM_SIZE + 5*TILE_SIZE, 0)
		print("Slot ", i, ": ", pos)
		current_rooms[i].position = pos


func is_slot_available(r : Node2D) -> bool:
	var id = r.name[-1] as int # thanks Rattman, I hate it
	var available = false
	if slot_available[id]:
		available = true
		slot_available[id] = false
	return available


func free_slot(slot : Node2D) -> void:
	var id = slot.name[-1] as int
	if not slot_available[id]:
		slot_available[id] = true
	else:
		print("Tentando liberar o slot ", id, "cuja liberdade ja existe")


func get_empty_room_slot() -> Node2D:
	for slot in current_rooms:
		if is_slot_available(slot):
			return slot
	return null


func get_slot_from_id(id : String) -> Node2D:
	for slot in current_rooms:
		if slot.get_child_count() > 0:
			var room = slot.get_child(0)
			if room.get_id() == id:
				return slot
	return null


func update_current_conveyors() -> void:
	var convs = get_tree().get_nodes_in_group("conveyors") # this is bad
	for conv in convs:
		# ugh this is O(n^2)
		conv.update_sister()


func room_load(id) -> void:
	print("Loading factory room ", id)
	var room_slot = get_empty_room_slot()
	if not room_slot:
		print("room slot é null, algo esta errado")
		return
	var room_instance = RoomTable.get_room_instance(id).instance()
	room_slot.call_deferred("add_child", room_instance)
	yield(get_tree(), "idle_frame")
#	room_slot.add_child(room_instance)
	update_current_conveyors()
	var room_creatures = CreatureTable.get_room_creatures(id)
	for c in room_creatures:
		spawn_creature(c, id, id)


func room_unload(id) -> void:
	var slot = get_slot_from_id(id)
	var room = slot.get_child(0)
	var room_creatures = CreatureTable.get_room_creatures(id)
	for c in room_creatures:
		var c_instance = CreatureTable.get_creature_instance(c)
		self.call_deferred("remove_child", c_instance)
#	slot.remove_child(room)
	room.queue_free()
	free_slot(slot)


func spawn_creature(creature_id, room_from, room_to) -> void:
	var creature = CreatureTable.get_creature_instance(creature_id)
	var room = get_slot_from_id(room_to).get_child(0)
	var pos = null
	
	if room_from == room_to: # spawns at random position
		pos = room.get_random_point()
		print("Spawn na posicao ", pos)
	else: # spawns at conveyor
		var conv = RoomTable.get_conveyor_destination(room_from, room_to)
		if conv == 0:
			print("Conveyor zero *NAO EXISTE* VAI CORRIGIR SEU CODIGO")
			return
		var convs = get_tree().get_nodes_in_group("conveyors")
		for c in convs:
			if c.id == conv:
				conv = c
				break
		if conv.available:
			conv.available = false
			pos = conv.warp_position
			conv.timer.start()
		print("Spawn na conveyor ", conv.id)
	
	if not pos:
		CreatureTable.update_room(creature_id, room_from)
		print("é, deu ruim mas td bem")
		return
	self.add_child(creature) # this should update the room
	creature.global_position = pos
	creature.update_room() # BUT
	if room_to == RoomTable.current:
		creature.set_aesthetics(true)
	else:
		creature.set_aesthetics(false)


func despawn_creature(creature_instance, room_from) -> void:
	if test_mode:
		print("criatura sumindo do ", room_from)
	self.remove_child(creature_instance)


func creature_moved(creature_instance, room_from, room_to) -> void:
	if test_mode:
		print(creature_instance.id, " foi de ", room_from, " para ", room_to)
	if room_to == RoomTable.current:
		creature_instance.set_aesthetics(true)
	else:
		creature_instance.set_aesthetics(false)
	creature_instance.update_room()


func spawn_object(room: String) -> void:
	var object_type = objects[room]
	if not object_type:
		return
	var cur_room = get_slot_from_id(room).get_child(0)
	var pos = cur_room.get_random_point()
	var prop = object_type.instance()
	objects[room] = null
	self.call_deferred("add_child", prop)
	prop.global_position = pos


func quit_game() -> void:
	$OfflineCreaturesTimer.stop()
	RoomTable.unload_all()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Menus/Main Menu/Menu.tscn")


func _on_OfflineCreaturesTimer_timeout():
	print("HORA DE ACORDAR, AMIGOS NAO-CARREGADOS")
	var unloaded_rooms : = RoomTable.get_unloaded_rooms()
	print("Os rooms que estao unloaded sao ", unloaded_rooms)
	for room in unloaded_rooms:
		var room_creatures : = CreatureTable.get_room_creatures(room)
		CreatureTable.run_creatures(room_creatures)
	print("pronto")


func _unhandled_input(event):
	if event.is_action_released("creature_table_print"):
		CreatureTable.pretty_print()
	if event.is_action_released("room_table_print"):
		RoomTable.pretty_print()
	if event.is_action_released("toggle_simulation"):
		if creatures_timer.is_paused():
			print("Continuando simulacao...")
			creatures_timer.paused = false
		else:
			print("PARO")
			creatures_timer.paused = true
	if event.is_action_released("ui_cancel"):
		quit_game()
